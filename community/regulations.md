---
title: Topluluk Kuralları
konqi: ../assets/img/konqi.png
sorted: 1
---

# Topluluk Kuralları

1. KDE Türkiye topluluk kanallarında yalnızca KDE ile ilgili konularda kalmaya özen gösterin.
2. Kimseye dili, dini, ırkı ve benzeri özelliklerden dolayı kişisel ithamlarda bulunmayın ve aşağılamayın.
3. Herhangi bir siyasal parti propagandası için yanlış yere geldiniz.
4. Herhangi bir dini yaymak veya başkalarını kendi yaşam tarzınıza uydurmaya çalışmak için yanlış yere geldiniz.
5. Türkçeyi düzgün kullanmaya özen gösterin.
6. Topluluk kanallarında müstehcen içerik paylaşmayın.
7. Topluluk kanallarında küfür içeren dil kullanmayın.
.
.
.
.
.

En önemlisi, düzgün bir insan değilseniz bile düzgün bir insan gibi davranmaya çalışın.



