---
title: Yerelleştirme
konqi: ../assets/img/konqi-support-document.png
sorted: 1
---

# KDE Türkçe Yerelleştirme Çalışmaları

Bu sayfada, KDE yazılımlarının çevirisine katkıda bulunmak için sizi en kısa sürede hazırlayacak yönergeler bulunmaktadır. Bu yönergeleri izledikten sonra, [Çeviri Yapmaya Başlamadan Önce](#çeviri-öncesi) bölümünden uymanız gereken kurallara ve en çok sorulan soruların yanıtlarına bakabilirsiniz.

## İçindekiler
1. [Posta Listesi Üyeliği](#posta-listesi)
2. [Çeviri Ortamı Kurulumu](#ortam-kurulumu)
3. [Lokalize Kullanımı](#lokalize-kullanımı)
4. [Gettext Hakkında](#gettext-hakkında)
5. [Çeviri Öncesi Yapılacaklar](#çeviri-öncesi)

## KDE Türkiye Posta Listesi Üyeliği {#posta-listesi}

Çeviri faaliyetleri, şu anda KDE Türkiye topluluğu e-posta listesi üzerinden koordine edilmektedir. [Bu adresten](https://mail.kde.org/mailman/listinfo/kde-l10n-tr) posta listesine üye olun ve kendinizi tanıtan ve neler üzerine çalışmak istediğinizi anlatan bir iletiyi listeye gönderin.

Çevirilerin geçerli tamamlanma durumlarına göz atmak veya belirli bir programın çeviri dizilerini indirmek için [KDE Türkiye Takım Sayfası](https://l10n.kde.org/team-infos.php?teamcode=tr)'na bakın. Buradan aynı zamanda KDE çevirileri ile ilgili birçok istatistiğe de ulaşabilirsiniz.

## Bilgisayarınıza Çeviri Ortamı Kurulumu {#ortam-kurulumu}

_Buradaki yönergeler, bir GNU/Linux türevi kullandığınızı varsaymaktadır._

- [Buradan](https://github.com/bitigchi/kde-l10n-tr-scripts/archive/refs/heads/master.zip), KDE Türkçe çeviri depolarını bilgisayarınıza klonlamanıza ve `Lokalize` uygulamasını kurup yapılandırmanıza yardımcı olacak betik dosyalarını indirin.
- İndirdiğiniz betik paketini açın ve çıkarılan klasöre bir uçbirim penceresinde geçin. KDE kullanıyorsanız çıkardığınız betik paketi dosyalarını içeren pencerede sağ tıklayın ve `Burada Uçbirim Aç`'ı seçin. Daha sonra aşağıdaki komutu yürütün:

```shell
$ chmod +x kde_tr_yapilandir.sh && ./kde_tr_yapilandir.sh
```

- Ekrandaki yönergeleri izledikten sonra `Lokalize` ve `Subversion` yazılımları kurulmuş ve KDE Türkçe çeviri depoları bilgisayarınıza klonlanmış olacak. Betik, sürecin sonunda size `Lokalize` uygulamasını açmak isteyip istemediğinizi soracak. `Enter` düğmesi ile `Lokalize`'yi açın.

## Lokalize Uygulamasını Kullanma {#lokalize-kullanımı}

`Lokalize` uygulaması, KDE teknolojilerini kullanan bir bilgisayar destekli çeviri yazılımıdır. Bu uygulama ile, KDE depolarını doğrudan programa yükleyebilir ve kullanışlı bir grafik arabirim ile kaynak diziler için çeviriler sağlayabilir ve var olan diziler arasında arama yapabilirsiniz.

`Lokalize` uygulamasını açtıktan sonra `Ayarlar` -> `Lokalize Uygulamasını Yapılandır` -> `Genel` adımlarını izleyin. Burada, sağ taraftaki paneldeki metin kutularını kullanarak kimlik bilgilerinizi ekleyip `Tamam`'a basın. Böylece, çeviri yaptığınız bileşenlerin üst verisine sizin kimlik bilgileriniz de eklenecek.

![Lokalize Yapılandırma Ekranı](../assets/loc/lokalize-yapılandır.png)

Bu işlemden sonra, sizi aşağıdaki gibi bir ekran karşılayacak. Bu ekranda, çeviriye hazır iki adet depo bileşeni görüyoruz; `messages` ve `docmessages`. `messages`, KDE uygulamalarının arayüz çevirilerinin bulunduğu alt bileşendir. `docmessages` ise KDE yazılımlarının yardım ve belgelendirme çevirilerini içerir.

![Lokalize Ana Ekran](../assets/loc/lokalize-proje-ayrıntı.png)

`messages` alt bileşeninin sol yanındaki oka tıklayarak alt klasörleri görünür duruma getirin. Ekran, aşağıdakine benzer olacaktır.

![Lokalize Bileşen Ayrıntıları](../assets/loc/lokalize-çevrilmemiş.png)

Burada, yeşil ile imlenen bileşenler çevirileri tamamlanmış dizileri, turuncu kısımlar ise daha önceden çevrilmiş; ancak çevrilmesinden bu yana üzerinde değişiklik yapıldığı halde hâlen güncellenmemiş dizileri, kırmızı kısımlar ise hiç çevrilmemiş dizileri belirtir.

Haydi, artık biraz çeviri yapalım. `Digikam` uygulamasının sol yanındaki oka tıklayarak kullanılabilir çeviri dosyalarını listeleyin ve `digikam.po` dosyasını açın. Sizi aşağıdaki gibi bir ekran karşılayacaktır.

![Lokalize Çeviri Ekranı](../assets/loc/lokalize-çeviri-ekranı.png)

Bu ekranda, `Lokalize` uygulamasının temel bileşenlerini görüyoruz. `Lokalize`, yapısı itibarıyla modüler bir uygulama olduğundan ekranda gördüğünüz kutucukları istediğiniz gibi taşıyabilir, yeniden boyutlandırabilir veya başlık çubuklarında sağ tıklayarak yeni modüller de ekleyebilirsiniz.

Sol taraftaki tablo görünümünde, halihazırda var olan kaynak dizileri ve hemen yanındaki sütünda hedef çevirileri görüyoruz. Bu örnekte, `2910` numaralı girdinin çevirisi tam görünüyor. `2909` numaralı girdinin ise hedef bölümü boş, demek ki henüz çevrilmemiş. Bu girdiye tıklayarak sağ tarafta çevirisini girebiliriz. `2908` numaralı girdi ise yatık metinle biçimlendirilmiş ve kaynak ve hedef çevirisi birbirini tutmuyor gibi görünüyor. Buradan, bu girdinin daha sonradan değiştirildiğini; ancak bizim henüz onu güncellemediğimizi anlayabiliriz.

Çevirilerinizi yaparken sık sık `Dosya` -> `Kaydet` (`CTRL` + `S`) kullanarak çalışmanızı kaydetmeyi unutmayın.

Bazen, daha önceden girdiğiniz bir çeviriyi değiştirmek veya bulduğunuz bir hatayı düzeltmek isteyebilirsiniz. Çeviri bileşenleri arasında arama yapmak için `Proje Genel Görünümü` sekmesindeyken `Düzen` -> `Bul...` (`CTRL` + `F`) yolunu izleyin. Açılan sekmede `Kaynak` veya `Hedef` metin kutularına istediğiniz metni girerek arama yapabilirsiniz. Sonuçlar, kısa bir beklemenin ardından aşağıdaki tabloda listeleneceklerdir.

![Lokalize Bul Ekranı](../assets/loc/lokalize-bul.png)

Aradığınız dizinin hangi dosyada olduğunu biliyorsanız o dosya açıkken aynı klavye kısayolunu kullanabilir veya kaynak/hedef dizi tablosunun hemen üst kısmında bulunan `Hızlı ara...` metin kutusunu kullanabilirsiniz.

Ekranınızın alt kısmındaki kutularda `Çeviri Belleği` diye bir bölüm de bulunmaktadır. `Lokalize`, işinizi kolaylaştırmak için aynı çevirileri yeniden girmeyesiniz diye onları belleğinde tutar. Eğer uygun bir girdi görürseniz `CTRL` + `1`, `2`, `3` kısayollarıyla onları doğrudan hedef çeviriye ekleyebilirsiniz.

Tebrikler, `Lokalize` uygulamasını kullanmayı öğrendiniz. Çeviri yapmaya ve çevirilerinizi göndermeye başlamadan önce üzerinden geçmemiz gereken birkaç konu daha var. Okumayı sürdürün!

## Gettext Yazılımına Genel Bir Bakış {#gettext-hakkında}

_`gettext` yazılımı ve dolayısıyla `po`, `pot` vb. dosyaları hakkında önceden bilginiz varsa ve değişken yer tutucusu nedir biliyorsanız bu bölümü [atlayabilirsiniz](#çeviri-öncesi)._

`Lokalize`, `POEdit` gibi yazılımlar, özünde `gettext` yazılımı tarafından derlenen ileti dizilerini alır ve kendi arabirimlerinde görüntülerler. Daha önceden klondığınız KDE çevirileri deposuna dosya sisteminizde bakarsanız `templates_kde6` ve `kde6_tr_trunk` gibi klasörler göreceksiniz. Bunların neler içerdiklerine bir bakalım:

- `templates_kde6`: KDE yazılımlarının arayüz iletileri şablonları
- `kde6_tr_trunk`: KDE Türkçe çevirilerinin ana depoları
- `kde5_tr_stable`: KDE Türkçe çevirilerinin kararlı sürüm depoları

İlk seçenekten, `messages` -> `ktechlab` -> `ktechlab.pot` yolunu izleyerek daha önceden çevrilmemiş bir uygulama olan `KTechlab` iletilerine bir bakalım:

```gettext
# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the ktechlab package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ktechlab\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-03 00:51+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: microbe/main.cpp:46 src/flowcodeview.cpp:39 src/ktechlab.cpp:500
#: src/textview.cpp:66
#, kde-format
msgid "Microbe"
msgstr ""

#: microbe/main.cpp:48
#, kde-format
msgid "The Microbe Compiler"
msgstr ""

#: microbe/main.cpp:50
#, kde-format
msgid "(C) 2004-2005, The KTechlab developers"
msgstr ""

...dosyanın geri kalanı
```
Dosyanın üst kısmında dosya üst verisi yer alır. Bu kısım, eğer düzgünce yapılandırılmışsa `Lokalize` uygulaması tarafından eklenir. Çeviri yapmaya başlamadan önce bu bilgileri `Lokalize` ayarlarında zaten girmiştik. Aşağı doğru ilerleyelim.

```gettext
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Işbara Alp"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "isbara-alp@kt.kt"
```

- `msgctxt`, verilen diziyi daha iyi anlamamız için, uygulama geliştiricileri tarafından eklenen yorum satırlarıdır
- `msgid`, kaynak çeviri dizisidir
- `msgtr`, hedef çeviri dizisidir; Türkçe çeviri, `Lokalize` tarafından buraya eklenir

Bu örneğe özel olarak, `Your names` ve `Your emails` kısımlarına, eğer uygulamanın çeviri teşekkürleri bölümünde adımınız çıkmasını istiyorsak kendi adımızı ve alttaki iletide e-posta adresimizi giriyoruz. Çeviriyi sürdürelim.

```gettext
#: microbe/main.cpp:46 src/flowcodeview.cpp:39 src/ktechlab.cpp:500
#: src/textview.cpp:66
#, kde-format
msgid "Microbe"
msgstr "Microbe"

#: microbe/main.cpp:48
#, kde-format
msgid "The Microbe Compiler"
msgstr "Microbe Derleyicisi"

#: microbe/main.cpp:50
#, kde-format
msgid "(C) 2004-2005, The KTechlab developers"
msgstr "(C) 2004-2005, KTechlab geliştiricileri"
```

Gördüğümüz üzere, `Lokalize` uygulaması, girdiğimiz çevirileri dosyaya böylece yansıtmış oldu.

Uygulamalar, genellikle kullanıcılar tarafından sağlanan veya başka konumlardan alınan verilerle çalışırlar; bu nedenle uygulamadaki tüm girdilerin böyle düz diziler olacağını düşünmek yanlış olur. Örneğin aşağıdaki iki diziye bakalım:

```gettext
#: microbe/microbe.cpp:93
#, kde-format
msgid "Could not open file '%1'\n"
msgstr ""
```

Bu satırın, uygulamanın arayüzünde şöyle göründüğünü varsayabiliriz:

> Could not open file 'dosya.txt'

`dosya.txt`, burada kullanıcı tarafından daha önceden kaydedilmiş bir dosyadır ve kaynak koduda `%1` değer yer tutucusu ile gösterilir. Satırın sonunda bir de `\n` olduğunu görüyoruz. Bu, Unix ve Unix-türevi işletim sistemlerinde yeni bir satıra geçildiğini ifade eden karakter dizisidir. Tüm bunları göz önüne alarak, çeviriyi şöyle yapıyoruz:

```gettext
#: microbe/microbe.cpp:93
#, kde-format
msgid "Could not open file '%1'\n"
msgstr "'%1' dosyası açılamadı\n"
```

Çeviri, arayüzümüzde böyle görünecek:

> 'dosya.txt' dosyası açılamadı

Değişken yer tutucuları, birden çok sayıda olabilir. Örneğin:

```gettext
#: src/flowparts/sevenseg.cpp:50
#, kde-format
msgid "Display %1 on %2"
msgstr "%2 üzerinde %1 görüntüle"
```

Türkçenin tümce ögeleri kuralları gereği, bu tür yer değiştirmeleri sık sık yapmamız gerekebilir.

Çeviri yaparken bazen `&` imini de pek sık görebiliriz. Örneğin:

```gettext
#. i18n: ectx: property (text), widget (QPushButton, m_pSaveButton)
#: src/gui/contexthelpwidget.ui:183
#, kde-format
msgid "S&ave"
msgstr &Kaydet""
```

`&` imi, öncesinde bulunan harfi klavye kısayolu olarak atar. `Alt` düğmesini basılı tutarak ayarlanmış klavye kısayolu düğmelerinin altında bir alt çizgi görebiliriz. Bu örnekte, `Alt` düğmesini basılı tutarak `K` harfine basmak, `Kaydet` eylemini tetikler. Klavye kısayolları atarken, diğer KDE uygulamalarındaki kısayollara önceden bir göz atın ve aynı/benzer kısayollar için harfleri tutarlı tutmaya çalışın.

Çevirilerimizi kaydettiğimiz anda, `.pot` uzantılı dosyalar `.po` olarak yeniden adlandırılırlar ve `kde5-tr-trunk` altındaki ilgili uygulamanın klasörüne yerleştirilirler. Çevirilerinizi topluluğa göndermeden önce onları canlı olarak deneyin ve düzeltilmesi gereken kısımları düzeltin. Bunu yapmak için, betik klasöründeki `ceviri_uygula.sh` betiğini kullanın ve yönergeleri izleyin.

```shell
$ chmod +x ceviri_uygula.sh # ilk kullanım için
$ ./ceviri_uygula.sh
```

__Not:__ Bu işlem, Flatpak kullanan uygulamalarda çalışmaz ve kullandığınız yazılımın ana deposundaki bazı iletiler değiştirilmişse tüm iletileri güncellemeyebilir.

Her şey yolundaysa artık güncellediğiniz `.po` dosyalarını e-posta listemize gönderebilirsiniz. Gönderdiğiniz çeviriler, KDE depolarına işleme yetkisi olan arkadaşlarımız tarafından denetlenip depoya işlenecektir. Katkıda bulunduğunuz için çok teşekkürler!

## Çeviri Yapmaya Başlamadan Önce {#çeviri-öncesi}

Aşağıda, çeviri yapmaya başlamadan önce uymanızın ve uygulamanızın yararlı olacağı birtakım pratikler ve bilgiler bulunmaktadır.

### KDE Yazılımları Kullanın

KDE, 20 yıldan fazladır var olan bir uygulama ekosistemi olarak, kendi terimbilimi ve pratikleri olan bir topluluktur. Bu nedenle, KDE'ye aşina olmanız tümüyle gerekli olmamakla birlikte kesinlikle önerilir. Halihazırda KDE Plasma ve diğer KDE yazılımlarını kullanıyorsanız süper; bir sonraki adıma geçebilirsiniz.

### Çevirileri Okuyun

Çevirisini yapmaya başlayacağınız uygulamanın tüm çevirilerini baştan aşağı okumak, uygulamada kullanılan dile ve jargona hakim olmanıza büyük katkı sağlayacaktır. Uygulamadaki çeviriler, tek bir dil ve anlatım üzerinden gitmelidir, böylece kullanıcının kafası karışmaz. Bununla ilgili ek maddelere aşağıda bakabilirsiniz.

- Terminolojiyi kullanın. `Lokalize` uygulaması, bu konuda size yardımcı olur. Eğer terminolojideki kavramlar istediğiniz bağlamı karşılamıyorsa kendi çevirinizi ekleyebilirsiniz.
- Eşanlamlı sözcüklere dikkat edin. Türkçemiz, eşanlamlı sözcükler bakımından varsıl bir dil olduğundan, aynı terimin farklı kişiler tarafından farklı biçimlerde çevrildiği pek sık görülen bir durum. Çeviride daha önceden hangi terim kullanıldıysa onu kullanmayı sürdürün.
- `Hızlı ara...` seçeneğini kullanarak aynı terimin hangi varyasyonlarda kullanıldığına her zaman bakın. Çeviri, aynı terimi farklı sonekler ve bağlaçlarla kullanmış olabilir. Her zaman kısa ve öz tutmaya çalışın.

### Çeviri Yaparken Bunlara Dikkat Edin

- Yeni bir çeviri yapmaya başlamadan önce her zaman deponuzu güncelleyin. Bunun için, `ceviri_guncelle.sh` betiğini kullanın.

```shell
$ chmod +x ceviri_guncelle.sh # ilk kullanım için
$ ./ceviri_guncelle.sh
```

Aksi taktirde, güncel olmayan dosyalarla çalışıyor olabilir ve çeviri koordinatörünün başını pek çok ağrıtabilirsiniz.

Aynı durum çeviri gönderme konusunda da geçerlidir. KDE gelişim süreci pek hızlı ilerlediğinden, her gece saat 03.00'te tüm depolar, kaynak uygulama kodlarındaki son değişikliklerle güncellenirler. Çeviri dosyası sürümünüz eğer eski kalırsa değişikliklerinizi birleştirmek ve işlemek çok daha zorlaşacaktır. Bu yüzden ufak parçalar halinde çalışın ve değişikliklerinizi günlük olarak gönderin.

- Uygulamada menü ve arayüz çevirilerinde ilk harflerin büyük yazımına dikkat edin. Uygulama genelinde hangi pratik sürdürülüyorsa onu kullandığınızdan emin olun. Örneğin, ana menü/bağlam menüsü çevirilerinde tüm sözcüklerin ilk harfinin büyük harfle başladığından emin olun. Aynı penceredeki dizilerin başlangıç harflerinin aynı biçemde olduğunu her zaman denetleyin.
- Değişken yer tutucularına kesme imi ile ek getirmeyin. Türkçenin Büyük Ünlü Uyumu dolayısıyla, getirdiğiniz ek ekrandaki değişkenle büyük olasılıkla farklı olacaktır. Bunun yerine şu pratikleri kullanın:

__YANLIŞ__: `%1'i Aç`
__DOĞRU__: `Aç: %1`

__YANLIŞ__: `%1'e Kopyala`
__DOĞRU__: `%1 Konumuna Kopyala`

Yer tutucunun hangi anlama geldiğinden emin değilseniz `Lokalize` uygulamasının ın `Birim Üst Verisi` penceresinden kaynak koduna bakabilir, kaynak kod okuyamıyorsanız posta listemizden sorabilirsiniz.

- Üç nokta kullanılan yerlerde üç adet tek nokta (`...`) değil, özel üç nokta karakterini (`…` – `U+2026`) karakterini kullanın.
- Tire kullanımı üzerine:
    - Sözcük/ad içindeki ayrımlarda tire (`-`)
    - Boşluksuz farklı kavramlar, tarihler, sayılar için en tiresi (`–`)
    - Boşluklu farklı kavramlar içinse em tiresi (`—`) kullanın.
- Düz tırnak işareti (`"`) veya kesme işareti (`'`) kullanmayın.
    - Alıntıları `“` ve `”` süslü tırnağı içine,
    - Terimleri `‘` ve `’` süslü tırnağı içine alın
- Matematiksel sembolleri harflerle veya birden çok karakterle belirtmekten kaçının, bunun yerine özel karakterleri kullanın; örneğin, çarpı işareti için `x` yerine `×`, büyük eşittir için `>=` yerine `≥` gibi.
- Makine çevirisi yapacaksanız şunlara dikkat edin ve düzeltmeleri yapın:
    - Kullanılan terimlerin KDE terminolojisine uygun olması
    - Aynı terimin aynı tümce içinde farklı sözcüklerle çevrilmemesi
    - BÜYÜK/küçük harf uyumları
    - Hitap biçimlerinin düzgün olması
        - Eylemler için tekil kişi (yap!)
        - Araç ipuçları için açıklayıcı ek metin yoksa tekil, varsa çoğul (yapın)
        - Nokta ile biten tümcelerde tek satırlık uçbirim çıktısı hariç çoğul (yapın)
- Yüzde imini kullanmanız gereken çevirilerde, KDE değişken yer tutucuları olarak kullanılan `%1`, `%2` gibi yapılarla çakışmayı engellemek için yüzde imi ve sayı arasında `ZERO WIDTH SPACE` (`U+200B`) kullanın. Örnek: `%<u+200b>50`. Ne yazık ki, KDE yazılımları yüzde içeren sayıları yerel ayarlara göre biçimlendirmeyi henüz desteklemiyor.





