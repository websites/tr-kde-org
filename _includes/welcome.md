# KDE Türkiye Topluluğu'na hoş geldiniz!

Topluluğumuza katılmak ve daha fazla bilgi almak için [Bize Katılın](/getinvolved) sayfasına bir göz atın!

Yaklaşık 20 yıldan bu yana kullanıcılarına Türkçe bir Linux masaüstü deneyimi sunan KDE'yi Türkiye'de daha fazla yaygınlaştırmak ve kullanıcı kitlesini artırmak için buradayız.

Topluluk Günlüğü'ne yazılarınızı bekliyoruz. Nasıl günlük yazısı ekleyeyebileceğiniz ile ilgili bilgileri [buradaki BENİOKU](https://invent.kde.org/websites/tr-kde-org) dosyasından öğrenebilirsiniz.

## Katkıda Bulunma Yolları

#### Çeviri yapabiliyorsanız

- Uygulama ve belgelendirme çevirilerine yardım edin!
- KDE Kullanıcı Vikisi çevirilerine yardım edin!

#### Kod yazabiliyorsanız

- KDE ekosistemi uygulamalarını geliştirmeye katkıda bulunun!
- KDE Türkçe yerelleştirme desteğini artırın!

#### Diğer katkı yolları

- Çevrenizde KDE kullanımını yaygınlaştırmaya çalışın!
