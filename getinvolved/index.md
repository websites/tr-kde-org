---
title: Bize Katılın
konqi: ../assets/img/konqi-commu-mail.png
sorted: 1
---

# İletişim Kanalları

## Posta Listesi

KDE Türkiye Topluluğu ile tanışmak ve topluluğa katılmak için [posta listemize](https://mail.kde.org/mailman/listinfo/kde-l10n-tr) üye olun!

Aynı zamanda, bizleri [Twitter](https://twitter.com/KDETurkiye) üzerinden de takip edebilirsiniz.

## Sohbet Kanalları

KDE Türkiye topluluğuna doğrudan ulaşmak ve iletişimde kalmak için aşağıdaki sohbet kanallarını kullanabilirsiniz. Tarayıcı üzerinden bağlanmak için kanalların üzerine tıklayın.

#### IRC Kanalı

Libera.Chat sunucusu üzerinde [#kde-tr](https://web.libera.chat/#kde-tr)

Sürekli bağlı kalmak için aşağıdaki Matrix kanalını/köprüsünü kullanın.

#### Matrix Kanalı

Kde.org sunucusu üzerinde [#kde-tr](https://matrix.org/#kde-tr:kde.org)

## RSS Beslemesi

Topluluk günlüğü gönderilerini diğer Türkçe KDE haberleriyle birlikte izlemek için [KDE Gezegeni Türkçe](https://planet.kde.org/tr/index.xml) RSS beslemesini kullanın (önerilir).

Yalnızca KDE Türkiye Topluluk Günlüğü'nü izlemek içinse [bu bağlantıyı](https://tr.kde.org/feed.xml) RSS besleyicinize ekleyin.

#### Önerilen RSS okuyucuları

- [Akregatör](https://apps.kde.org/tr/akregator/)
- [Newsboat](https://newsboat.org)
