# tr.kde.org

KDE Turkey Community Website / KDE Türkiye Topluluk Web Sitesi

## Topluluk günlüğüne yazı ekleme

- Öncelikle, `_authors` dizinine kendi adınızı ve kısa yazar kodunuzu içeren bir dosya ekleyin. Buraya, kendinizi tanıtan bilgiler de girebilirsiniz.
- `_posts` dizini altına yazmak istediğiniz yazıyı koyun. Dosya adı ve üstbilgi için önceki örnekleri izleyin.
- İlgili etiketleri koymayı unutmayın.
- Yeni bir birleştirme isteği açın

Siteyi, yerel olarak derleyip görüntülemek içinse aşağıdaki adımları izleyin:

## Derleme ortamı kurulumu

### Yazılımlar

- Ruby 2.5.0 ve üstü
- [Jekyll 4.4.2](https://jekyllrb.com/docs/installation) ve üstü

### Jekyll kullanımı

Jekyll'i bilgisayarınıza kurduktan sonra, bu depoya git yardımıyla klonlayın veya `.zip` olarak bilgisayarınıza indirin. Daha sonra, aşağıdaki komutları kullanın:

```shell
$ cd /path/to/tr-kde-org/
$ bundle install
$ bundle exec jekyll serve
```

Değişikliklerin kendiliğinden yeniden yüklenmesini istiyorsanız şu komutu kullanın:

```shell
$ bundle exec jekyll serve --livereload
```
