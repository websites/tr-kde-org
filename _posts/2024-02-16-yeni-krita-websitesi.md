---
layout: post
title: Yeni Krita web sitesi
author: esari
tags: ["Topluluk", "Çeviri", "KDE", "Krita"]
---
Bu, uzunca bir süredir teknik konulardan dolayı kuluçkada bekliyordu; ancak artık en sonunda Krita’nın yeni bir web sitesi var!

![Krita web sayfası ekran görüntüsü](/assets/img/krita/krita-web-sitesi-2024-02-15.png){:width="100%"}


Çevrilmiş resmi duyuruda da belirtildiği üzere, site Hugo tabanlı ve statik yapısıyla çok kolay yeni yazıların eklenmesine ve çevrilmesine olanak tanıyor. Böylelikle yalnızca sitenin arayüzü değil, tüm haber/yazı içeriğini de Türkçe sunmak olanaklı.

Site henüz çok yeni olduğundan bazı yerlerde kırık bağlantılar veya çeviride ufak-tefek anlatım bozuklukları olabilir. Zamanla daha da düzelecektir.

Site arayüzünün yanında Krita, arayüzü %100 ve yardım çevirilerinin de yarısından çoğu Türkçeleştirilmiş ve tümüyle ücretsiz olarak kullanıma açık! Sayısal boyama/çizim ile uğraşıyorsanız ve terminolojiye tümüyle hakimseniz çevirilerde geri bildirimlerinizi her zaman bekleriz.

