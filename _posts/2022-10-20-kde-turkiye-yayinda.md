---
layout: post
title:  KDE Türkiye web sayfası yeniden yayında!
author: esari
tags: ["KDE Türkiye", "Web Sitesi"]
---

Linux ortamındaki en bilinen masaüstü ortamlarından birisi olan KDE'nin ülkemizdeki geçmişi yaklaşık 20 yıldan fazla bir zaman öncesine dayanıyor. Yerelleştirme desteğinin eklendiği 3.0 sürümünden itibaren, KDE Türkiye topluluğu da bizlere tümüyle Türkçe bir KDE sunmak için bu işe yıllarını verdi; ancak Türkiye'deki KDE topluluğunin bir konferans veya başka bir etkinlik düzenleyecek kadar büyüyüyebildiğini söylemek yanlış olur.

KDE Türkiye Topluluk web sayfasını yeniden yayıma alarak, hem KDE yerelleştirmesi çalışmalarını bir merkeze toplamak hem de genel olarak topluluğu genişletmek için ilk adımı atmış olduk.

## Bu sayfanın amacı nedir?

KDE Türkiye web sayfasını birkaç amaçla kullanmayı planlıyoruz:

- KDE Türkçe çevirilerine katkıda bulunmak isteyenler için bir toplanma noktası
- Çevirilerin nice yapılması gerektiği konusunda bir kılavuz sayfası
- KDE Türkiye topluluğunun iletişim bilgilerine doğrudan ulaşma merkezi
- KDE Türkiye topluluğunun genel olarak KDE ve KDE Plasma ile ilgili paylaşımları için bir toplanma noktası

## Topluluk günlüğü hakkında

Topluluk Günlüğü ile, KDE ile ilgili tüm paylaşmak istediğiniz yazıları buradan gönderebilirsiniz. Bu yazılar, doğrudan [KDE Gezegeni](https://planet.kde.org)'nin Türkçe bölümüne de bağlanıyor; böylece tüm KDE Gezegeni'nden anında takip edilebiliyor.

## IRC ve Matrix kanalları

Topluluk ile doğrudan iletişime geçmek ve bir toplanma noktasında buluşmak için [IRC](https://libera.chat/#kde-tr) ve [Matrix](https://matrix.org/#kde-tr:kde.org) kanallarını kullanabilirsiniz. Özünde ikisi de aynı yere çıkıyor, böylece; örneğin bilgisayar başındayken daha yalın olan IRC'yi, taşınabilir aygıtınızda ise sürekli bağlı kalmak için Matrix bağlantısı bulunmaktadır.
