---
layout: post
title: Plasma 5.27 ve Türkçe KDE üzerine
author: esari
tags: ["Topluluk", "Çeviri", "KDE", "KDE Plasma", "KDE Gear"]
---
*6 Şubat 2023 Gaziantep-Kahramanmaraş depremlerinde yaşamlarını yitiren tüm yurttaşlarımızı en derinden ve saygıyla anıyoruz; hepimizin başı sağ olsun. Türk ulusu, geçmişte yaşadığı her yıkımdan başı dik çıkmasını bilmiştir ve bundan da çıkacaktır. Birliğimiz bengi olsun.*

\*\*\*

KDE Plasma 5.27, dört aylık bir aradan sonra bugün yayımlandı. Ayrıntılı tanıtım sayfasına ve sürüm notlarına Türkçe olarak [bu sayfadan](https://kde.org/tr/announcements/plasma/5/5.27.0/) ulaşabilirsiniz.

Bu sürümün Türkçe KDE kullanıcıları için önemi ise uzun yıllardan beridir ilk kez tam bir Türkçe deneyim sunması. Geçtiğimiz 2022 yılında, KDE Türkiye topluluğunun özverili çalışmalarıyla KDE uygulamaları, kitaplıkları ve web sitelerinin çevirileri büyük bir oranda tamamlandı. Rakamlara dökecek olursak, toplam **569** adet KDE paketinden yalnızca **17** tanesinin çevirilerinde eksikler bulunuyor; bu sayının içinden 4'ü web sayfası, 4'ü belgelendirme çevirisi ve kalan 9 tanesi KDE ekosisteminde yer alan bazı uygulamalar.

KDE'nin Türkçe serüveni, KDE 3.0'ın yerelleştirme desteği sunmasının ardından o dönemki Türkçe Linux hareketinin önde gelenlerinin çabalarıyla başladı. KDE özelinde konuşacak olursak Görkem ÇETİN ve Serdar SOYTETİR'in büyük katkılarıyla KDE uygulamaları ve K Masaüstü Ortamı, Türkçe olarak Linux kullanıaılarına sunuldu. Biraz önce söz ettiğim kişilerin dışındaki tüm çeviri katkıcılarına da teşekkürlerimizi sunarız. Aşağıda o zamanlardan kalma bir ekran görüntüsü demetini bulabilirsiniz. Tam boyutları görüntülemek için sağ tıklayıp yeni sekmede/pencerede açın.

![Ekran Görüntüsü 1](/assets/posts/2023/02/ss7.jpg){:width="50%"}
![Ekran Görüntüsü 2](/assets/posts/2023/02/ss8.jpg){:width="50%"}
![Ekran Görüntüsü 3](/assets/posts/2023/02/ss9.jpg){:width="50%"}
![Ekran Görüntüsü 4](/assets/posts/2023/02/ss10.jpg){:width="50%"}

Daha sonraları bayrağı devralan başta Kaan ÖZDİNÇER, Necdet YÜCEL ve Volkan GEZER ve diğer topluluk üyelerinin katkılarıyla KDE yazılımlarının Türkçeleştirilmesi sürdürüldü. KDE yazılımlarının son yıllarda atağa geçen inanılmaz geliştirme hızı ve yeni eklenen KDE uygulamaları sonucunda 2020'li yıllara geldiğimizde KDE uygulamalarının Türkçeleştirme oranı ne yazık ki %50'lere varan oranlara kadar düştü ve uygulamalar ve Plasma kabuğunda kullanıcı deneyimini etkileyecek oranda çeviri eksikliği hissedilir bir duruma geldi.

Topluluğumuzun geçen yılki atağı ile artık %80 düzeyinde bir Türkçeleştirme oranına sahibiz. Kalan %20'lik oranın büyük bir bölümü belgelendirme çevirileri ve web sayfasındaki eski yazılım sürüm notlarından oluşuyor. Bunları çıkarırsak neredeyse %98-%99 oranında KDE yazılımları artık Türkçe. Ayrıntılı bilgi ve hangi bileşenlerin çevirilerinin eksik olduğunu görmek için [bu sayfaya](https://l10n.kde.org/stats/gui/trunk-kf5/team/tr/) bakabilirsiniz.

2022 yılında yapılanlar yalnızca yeni çevirilerle sınırlı değil. Var olan tüm çeviriler en baştan başlayarak satır satır elden geçirildi ve tüm kullanılan terminolojinin yekpare olması sağlandı. Eskiden aynı terimin aynı uygulama içinde 3-4 farklı çevirisine rastlarken, artık her bir terim tüm KDE ekosisteminde tek bir kavramla karşılanıyor; böylece neyin neye denk geldiğini aklınız karışmadan anlayabilirsiniz! Elden geçirilen toplam dizi sayısı an itibarıyla 217.000 civarında. Bunu sözcük hesabına dökecek olursak milyonlarca elden geçirilen ve yeniden düzenlenen sözcükten söz ediyoruz.

Bu işlem süresince, yıllardır birikmiş hataların hepsi düzeltildi ve yazılımlar gerektiğinde anlam bütünlüğüne sahip olmaları için en baştan çevrildi.

Yine de anlaşılmayan kısımlar mı var? Kişioğlu hata yapar, biz de hata yapmış olabiliriz tabii ki. Denk geldiğiniz hataları [KDE Türkiye Twitter adresinden](https://twitter.com/KDETurkiye) veya [KDE Türkiye Posta Listesinden](https://mail.kde.org/mailman/listinfo/kde-l10n-tr) topluluğumuza bildirin! Özellikle profesyonel olarak uğraşınız olan bir alanda çevirilere katkıda bulunmak isterseniz tam aradığımız kişi sizsiniz!

KDE Türkiye sitesinde çevrilmiş görünen bir uygulamada hâlâ eksik çeviriler görüyorsanız bunun gerekçesi şu olabilir: Kimi uygulamaların yeni sürümlerinin çıkması bazen yılları bulabiliyor. Bu nedenden dolayı çevrilen iletiler henüz yeni sürümle son kullanıcıya ulaşmamış olabilir. Böyle bir uygulamaya denk gelirseniz bizimle iletişime geçin; Linux üzerinde yeni çevirilerin `gettext` uygulaması yardımıyla kullanılması olanaklıdır.

Plasma 6.0 sürümüne yaklaşırken, sonraki ereğimiz, Plasma 6.0'daki belgelendirme (Uygulama El Kitapları, Web Sayfaları) çevirisinin en azından temel sistem ile gelen kısımlarının hepsini tamamlamak. Böylece, bir uygulama hakkında destek almanız gereken bir durumda ilk olarak `Yardım``Uygulama El Kitabı` seçeneğine başvurarak doğrudan bilgiye ulaşabileceksiniz.

Twitter demişken, atıl durumdaki [KDE Türkiye Topluluğu Twitter Adresi](https://twitter.com/KDETurkiye) yeniden etkin duruma getirildi ve artık elimizden geldiğince yeni yazılım yayım duyurularını Türkçe olarak buradan paylaşıyoruz. Bunun dışında, arada sırada KDE yazılımlarının reklamlarını da buradan görebilirsiniz.

Ek olarak, KDE TÜrkiye topluluk web sayfası da yeniden hizmete alındı! Eskiden [kde.org.tr](https://www.kde.org.tr) üzerinde hizmet veren sayfa uzun yıllardan beri atıl durumda olduğundan [`tr.kde.org`](https://tr.kde.org) alan adı açıldı. Sitemize blog yazılarıyla ve aklınıza gelebilecek her türlü içerikle katkıda bulunabilirsiniz. Canlı bir KDE Türkiye topluluğu yaratmak adına toplu katılımın önemi çok büyük.

Yazıyı bitirmeden önce biraz da KDE yazılımlarından söz edelim. KDE yazılımları ekosistemi, 30 yıllık geliştirmenin sonucunda profesyonel kalitede birçok uygulama barındırıyor. [KDE Kişisel Veri Yönetimi Takımı](https://kontact.kde.org/tr/), [Kdenlive](https://kdenlive.org/en/), [Krita](https://krita.org), [Labplot](https://labplot.kde.org) bunlardan yalnızca birkaçı. Bu uygulamaların hepsi birbiriyle tümleştiğinden, Linux ve BSD üzerinde tümüyle yekpare bir kullanıcı deneyimi almanız olanaklı! İleride, bu uygulamaları bireysel olarak ele alan blog yazılarıyla karşınızda olacağız.

En son KDE Türkçe haberlerini almak için [KDE Gezegeni](https://planet.kde.org/tr/)'nin [RSS beslemesine](https://planet.kde.org/tr/index.xml) kayıt olun. Böylece haberleri doğrudan yeğlediğiniz RSS beslemesi okuyucuyla doğrudan alabilirsiniz. [Akregatör](https://kontact.kde.org/tr/components/akregator/)'ü kullanmanızı öneririz!

Plasma 5.27'den birkaç ekran görüntüsü de ekleyelim de tam olsun.

![Ekran Görüntüsü 5](/assets/posts/2023/02/ss1.png){:width="100%"}
![Ekran Görüntüsü 6](/assets/posts/2023/02/ss2.png){:width="100%"}
![Ekran Görüntüsü 7](/assets/posts/2023/02/ss3.png){:width="100%"}
![Ekran Görüntüsü 8](/assets/posts/2023/02/ss4.png){:width="100%"}
![Ekran Görüntüsü 9](/assets/posts/2023/02/ss5.png){:width="100%"}
![Ekran Görüntüsü 10](/assets/posts/2023/02/ss6.png){:width="100%"}

