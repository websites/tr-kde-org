---
layout: post
title:  Plasma 6 Bağış Kampanyası
author: esari
tags: ["Topluluk", "Çeviri", "KDE", "Plasma", "Bağış"]
---

Önümüzdeki 2024 yılının Şubat ayında çıkması tasarlanan Plasma Masaüstü Ortamı'nın 6. sürümü için KDE topluluğu bir bağış kampanyası başlattı. Kampanyanın ereği, lansman tarihine kadar 500 destekçi üyeye ulaşmak. An itibarıyla 122 üye sayısına ulaşıldı. Yıllar boyu yaklaşık 50 üye civarında takılan KDE topluluğu için bu şimdiden büyük bir başarı.

Kampanya hakkında ayrıntılı bilgiye [bu özel kampanya sayfasından](https://kde.org/tr/fundraisers/plasma6member/) sayfasından ulaşabilirsiniz. Sayfa tümüyle Türkçeye çevrildi ve bir canlı sayaç da içeriyor. Bağış kutusunu sağlayan DonorBox henüz bir Türkçe çeviri sunmasa da tarayıcınızın çeviri özelliğini kullanabilirsiniz. Türkçe çeviri isteği için [buradan](https://donorbox.org/contact) DonorBox'a yazın.

Finansal durumunuz KDE'ye doğrudan bağış yapmaya elveriyorsa lütfen bağış yapmayı düşünün. Başka büyük özgür yazılım projelerinin aksine, KDE'nin kurumsal desteği yok denecek kadar az ve KDE topluluğu kısıtlı olanaklarla çok büyük işler başarıyorlar. Bunu ileriye taşımak için yardımınıza gereksinimimiz var!
